# OpenML dataset: Amazon-10Year-Stock-Data-Latest1997-2020

https://www.openml.org/d/43637

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Amazon has become a house hold name now and has been around for quite sometime. It comes under the popular FAANG companies and a dream company for many. Today almost anything that you need today is available on Amazon. From groceries to electronics. But it has not only benefited the people purchasing from them. It has benefited those too who invested in the company back then and continue to do till today.
Content
This data set has 7 columns with all the necessary values such as opening price of the stock, the closing price of it, its highest in the day and much more. It has date wise data of the stock starting from 1997 to 2020(August).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43637) of an [OpenML dataset](https://www.openml.org/d/43637). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43637/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43637/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43637/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

